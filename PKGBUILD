# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Denis Klester <dinisoft@gmail.com>

_module=rtl8723bu
pkgname="${_module}-dkms"
pkgver=20240303
pkgrel=1
pkgdesc="Driver for RTL8723BU"
arch=('any')
url="https://github.com/lwfinger/rtl8723bu"
license=('GPL')
depends=('dkms')
makedepends=('git')
_commit=af3a408d6399655b0db23c2c8720436ca725ca47  # master
source=("git+https://github.com/lwfinger/rtl8723bu.git#commit=${_commit}"
        'blacklist-rtl8xxxu.conf'
        'dkms.conf')
sha256sums=('a9ff148d393079de8618db758905d42e48f098e6c7ebd5b12518f4e4a1117da2'
            '7c726ad04083c8e620bc11c837e5f51d3e9e2a5c3e19c333b2968eb39f1ef07e'
            '9c5dca33a2e4531ecb892b7a57feb93a2f2d5936dea81d3f879ad5831976f6b2')

pkgver() {
    cd "${_module}"
    git show -s --date=format:'%Y%m%d' --format=%cd
}

prepare() {
    cd "${_module}"
    rm -f *.tar.gz
}

package() {
    local install_dir="${pkgdir}/usr/src/${_module}-${pkgver}"

    # Copy dkms.conf
    install -Dm644 dkms.conf "${install_dir}/dkms.conf"

    # blacklist rtl8723bu
    install -Dm644 blacklist-rtl8xxxu.conf "${pkgdir}"/etc/modprobe.d/blacklist-rtl8xxxu.conf

    # Set name and version
    sed -e "s/@_PKGBASE@/${_module}/" \
        -e "s/@PKGVER@/${pkgver}/" \
        -i "${install_dir}/dkms.conf"
    cd "${srcdir}/${_module}"
    rm -r ".git"{,ignore}
    for d in $(find . -type d); do
        install -dm755 "${install_dir}/$d"
    done
    for f in $(find . -type f); do
        install -m644 "$f" "${install_dir}/$f"
    done
    sed -i 's/EXTRA_CFLAGS += -DCONFIG_CONCURRENT_MODE/#EXTRA_CFLAGS += -DCONFIG_CONCURRENT_MODE/g' ${install_dir}/Makefile
}
